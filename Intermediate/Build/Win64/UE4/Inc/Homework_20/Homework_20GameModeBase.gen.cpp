// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Homework_20/Homework_20GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHomework_20GameModeBase() {}
// Cross Module References
	HOMEWORK_20_API UClass* Z_Construct_UClass_AHomework_20GameModeBase_NoRegister();
	HOMEWORK_20_API UClass* Z_Construct_UClass_AHomework_20GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Homework_20();
// End Cross Module References
	void AHomework_20GameModeBase::StaticRegisterNativesAHomework_20GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AHomework_20GameModeBase_NoRegister()
	{
		return AHomework_20GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AHomework_20GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AHomework_20GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Homework_20,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHomework_20GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Homework_20GameModeBase.h" },
		{ "ModuleRelativePath", "Homework_20GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AHomework_20GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AHomework_20GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AHomework_20GameModeBase_Statics::ClassParams = {
		&AHomework_20GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AHomework_20GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AHomework_20GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AHomework_20GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AHomework_20GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AHomework_20GameModeBase, 3861597680);
	template<> HOMEWORK_20_API UClass* StaticClass<AHomework_20GameModeBase>()
	{
		return AHomework_20GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AHomework_20GameModeBase(Z_Construct_UClass_AHomework_20GameModeBase, &AHomework_20GameModeBase::StaticClass, TEXT("/Script/Homework_20"), TEXT("AHomework_20GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHomework_20GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
