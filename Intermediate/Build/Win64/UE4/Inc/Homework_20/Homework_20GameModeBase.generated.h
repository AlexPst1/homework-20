// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK_20_Homework_20GameModeBase_generated_h
#error "Homework_20GameModeBase.generated.h already included, missing '#pragma once' in Homework_20GameModeBase.h"
#endif
#define HOMEWORK_20_Homework_20GameModeBase_generated_h

#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_SPARSE_DATA
#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_RPC_WRAPPERS
#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHomework_20GameModeBase(); \
	friend struct Z_Construct_UClass_AHomework_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AHomework_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Homework_20"), NO_API) \
	DECLARE_SERIALIZER(AHomework_20GameModeBase)


#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAHomework_20GameModeBase(); \
	friend struct Z_Construct_UClass_AHomework_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AHomework_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Homework_20"), NO_API) \
	DECLARE_SERIALIZER(AHomework_20GameModeBase)


#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHomework_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHomework_20GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHomework_20GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHomework_20GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHomework_20GameModeBase(AHomework_20GameModeBase&&); \
	NO_API AHomework_20GameModeBase(const AHomework_20GameModeBase&); \
public:


#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHomework_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHomework_20GameModeBase(AHomework_20GameModeBase&&); \
	NO_API AHomework_20GameModeBase(const AHomework_20GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHomework_20GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHomework_20GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHomework_20GameModeBase)


#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_12_PROLOG
#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_SPARSE_DATA \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_RPC_WRAPPERS \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_INCLASS \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_SPARSE_DATA \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Homework_20_Source_Homework_20_Homework_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK_20_API UClass* StaticClass<class AHomework_20GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Homework_20_Source_Homework_20_Homework_20GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
