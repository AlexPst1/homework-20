// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK_20_PlayerPawnBase_generated_h
#error "PlayerPawnBase.generated.h already included, missing '#pragma once' in PlayerPawnBase.h"
#endif
#define HOMEWORK_20_PlayerPawnBase_generated_h

#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_SPARSE_DATA
#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandleHorisontalPlayerInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandleHorisontalPlayerInput(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHandleVerticalPlayerInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandleVerticalPlayerInput(Z_Param_value); \
		P_NATIVE_END; \
	}


#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandleHorisontalPlayerInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandleHorisontalPlayerInput(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHandleVerticalPlayerInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandleVerticalPlayerInput(Z_Param_value); \
		P_NATIVE_END; \
	}


#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Homework_20"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Homework_20"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public:


#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawnBase)


#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Homework_20_Source_Homework_20_PlayerPawnBase_h_12_PROLOG
#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_SPARSE_DATA \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_RPC_WRAPPERS \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_INCLASS \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Homework_20_Source_Homework_20_PlayerPawnBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_SPARSE_DATA \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
	Homework_20_Source_Homework_20_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK_20_API UClass* StaticClass<class APlayerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Homework_20_Source_Homework_20_PlayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
