// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Homework_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_20_API AHomework_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
